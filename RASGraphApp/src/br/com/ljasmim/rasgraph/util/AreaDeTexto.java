package br.com.ljasmim.rasgraph.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;

/**
 * Classe utilitária que implementa a interface Runnable, permitindo que uma
 * threads alterne a visibilidade de JTextArea enquanto uma solicitação é
 * executada em segundo plano.
 *
 * @author LeonardoJasmim
 */
public class AreaDeTexto implements Runnable {

    private final JTextArea tela;
    private final String path;

    public AreaDeTexto(JTextArea tela, String path) {
        this.tela = tela;
        this.path = path;
    }

    @Override
    public void run() {
        lerArquivoDeLog();
    }

    public void lerArquivoDeLog() {
        tela.setEnabled(false);
        try {
            Scanner in;
            in = new Scanner(new FileReader(path));
            while (in.hasNextLine()) {
                String line = in.nextLine();
                tela.setText(tela.getText() + line + "\n");
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AreaDeTexto.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
