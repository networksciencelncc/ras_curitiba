package br.com.ljasmim.rasgraph.util;

import br.com.ljasmim.rasgraph.app.RASGraphConverter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;

/**
 * Classe utilitária que implementa a interface Runnable, permitindo que as
 * rotinas de conversão sejam iniciadas enquanto
 * outra thread gerencia o JTextArea.
 * 
 * @author LeonardoJasmim
 */
public class Conversor extends RASGraphConverter implements Runnable{

    JTextArea tela;
    List<String> relativePath;
    
    public Conversor(JTextArea tela, List<String> relativePath) throws FileNotFoundException {
        super();
        this.tela = tela;
        this.relativePath = relativePath; 
    }    
    
    @Override
    public void run() {
        new Thread(new AreaDeTexto(tela, logPath)).start();
        try {
            start(this.relativePath);
        } catch (IOException ex) {
            Logger.getLogger(Conversor.class.getName()).log(Level.SEVERE, null, ex);
        }
        new Thread(new AreaDeTexto(tela, logPath)).start();
    }
    
}
