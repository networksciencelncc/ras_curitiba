package br.com.ljasmim.rasgraph.view.desktop;

import br.com.ljasmim.rasgraph.app.RASGraphConverter;
import br.com.ljasmim.rasgraph.util.Conversor;
import br.com.ljasmim.rasgraph.util.Util;
import java.io.File;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Tela da Aplicação RASGRAPH
 *
 * @author LeonardoJasmim
 */
public class Main extends javax.swing.JFrame {
    
    public Main() {
        initComponents();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        spnlTela = new javax.swing.JScrollPane();
        txtTela = new javax.swing.JTextArea();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuItemSelecionar = new javax.swing.JMenuItem();
        menuItemInterromper = new javax.swing.JMenuItem();
        menuItemSair = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("RASGRAPH - Conversor de informações sobre Rede de Atenção à Saúde");
        setPreferredSize(new java.awt.Dimension(800, 400));
        setResizable(false);

        txtTela.setColumns(20);
        txtTela.setRows(5);
        spnlTela.setViewportView(txtTela);

        menuFile.setText("File");

        menuItemSelecionar.setText("Selecionar arquivo");
        menuItemSelecionar.setToolTipText("<html>\nSelecionar arquivo .txt com caminho relativo para os arquivos .csv\n</html>");
        menuItemSelecionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemSelecionarActionPerformed(evt);
            }
        });
        menuFile.add(menuItemSelecionar);

        menuItemInterromper.setText("Interromper conversão");
        menuItemInterromper.setToolTipText("Interrompe o processo de conversão.");
        menuFile.add(menuItemInterromper);

        menuItemSair.setText("Sair");
        menuFile.add(menuItemSair);

        menuBar.add(menuFile);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(spnlTela, javax.swing.GroupLayout.DEFAULT_SIZE, 770, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(spnlTela, javax.swing.GroupLayout.DEFAULT_SIZE, 320, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void menuItemSelecionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemSelecionarActionPerformed
        selecionarArquivoComEnderecoDoCsv();
        start();
    }//GEN-LAST:event_menuItemSelecionarActionPerformed
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenuItem menuItemInterromper;
    private javax.swing.JMenuItem menuItemSair;
    private javax.swing.JMenuItem menuItemSelecionar;
    private javax.swing.JScrollPane spnlTela;
    private javax.swing.JTextArea txtTela;
    // End of variables declaration//GEN-END:variables

    File file;
    
    private void selecionarArquivoComEnderecoDoCsv() {
        JFileChooser chooser = new JFileChooser();
        chooser.setFileFilter(new FileNameExtensionFilter("Arquivo de Texto", "txt"));
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            file = chooser.getSelectedFile();
        }
    }
    
    public void start() {
        try {
            Conversor conversor;
            List<String> relativePaths = Util.getRelativePathsFromFile(file.getAbsolutePath());
            new Thread(new Conversor(txtTela, relativePaths)).start();
        } catch (Exception ex) {
            txtTela.setText(txtTela.getText() + "\n" + ex.getMessage());
        }
    }
    
}
